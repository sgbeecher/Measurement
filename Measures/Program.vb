﻿Module Program

    Sub Main()
        Dim m  = New Measure()
        Dim m2  = New Measure()

        Dim types = [Enum].GetValues(GetType( Measure.MeasureType)).Cast(of Measure.MeasureType).OrderByDescending(function(type) type).ToList()
        For Each t As Measure.MeasureType In types
            m.SetMeasure(t, 1)   
            Console.WriteLine($"Add: {m}")
            m2 = m2.Add(t, 1)  
            Console.WriteLine($"Result: {m2}")
        Next

        For Each t As Measure.MeasureType In types
            m.SetMeasure(t, 1)   
            Console.WriteLine(m)
            Console.WriteLine($"Quartered = {m.MultiplyBy(0.25)}")
            Console.WriteLine($"Third = {m.MultiplyBy(0.33333)}")
            Console.WriteLine($"Halved = {m.MultiplyBy(0.5)}")
            Console.WriteLine($"Doubled = {m.MultiplyBy(2)}")
        Next
        
        Console.ReadLine()
    End Sub

End Module
