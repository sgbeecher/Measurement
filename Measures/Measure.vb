﻿Imports System.Text
Imports System.Linq

Public Class Measure
    Private const PinchConversion = 1
    Private const TeaspoonQuarterConversion = PinchConversion * 4
    Private const TeaspoonHalfConversion = TeaspoonQuarterConversion * 2
    Private const TeaspoonConversion = TeaspoonHalfConversion * 2
    Private const TablespoonConversion = TeaspoonConversion * 3
    Private Const CupConversion = TablespoonConversion * 16
    Private Const CupQuarterConversion = CupConversion * (1/4)
    Private Const CupThirdConversion = CupConversion * (1/3)
    Private Const CupHalfConversion = CupConversion * (1/2)
    Private Const CupTwothirdConversion = CupConversion * (2/3)
    Private Const CupThreequarterConversion = CupConversion * (3/4)
    Private Const PintConversion = CupConversion * 2
    Private Const QuartConversion = PintConversion * 2
    Private Const GallonConversion = QuartConversion * 4
    
    ''' <summary>
    ''' All measurements are made based on a "pinch" which is abitrarily being set
    ''' </summary>
    enum MeasureType 
        Gallon = GallonConversion
        Quart = QuartConversion
        Pint = PintConversion

        Cup = CupConversion
        CupThreeQuarter = CupThreequarterConversion
        CupTwoThird = CupTwothirdConversion
        CupHalf = CupHalfConversion
        CupThird = CupThirdConversion
        CupQuarter = CupQuarterConversion
        
        Tablespoon = TablespoonConversion
        Teaspoon = TeaspoonConversion
        TeaspoonHalf = TeaspoonHalfConversion
        TeaspoonQuarter = TeaspoonQuarterConversion
        Pinch = PinchConversion
    End enum

    private Dim ReadOnly Values as Dictionary(Of MeasureType, Integer)
    Private Dim Teaspoons as Integer

    public Sub New ()
        Values = New Dictionary(Of MeasureType,Integer)()
        Teaspoons = 0 
    End Sub

    public Sub New (tsps As Integer) 
        Me.new()
        SetTeaspoons(tsps)
    end Sub

    
    ''' <summary>
    ''' Set the measurement as a decimal value
    ''' </summary>
    ''' <param name="mType"></param>
    ''' <param name="value"></param>
    public sub SetMeasure(mType as MeasureType, value as Double)
        SetTeaspoons(mType * value)
    end sub

    ''' <summary>
    ''' Add a measuremnet 
    ''' </summary>
    ''' <param name="mType"></param>
    ''' <param name="value"></param>
    public function Add(mType as MeasureType, value as Double)
        Return New Measure(Teaspoons + (mType * value))
    end function

    ''' <summary>
    ''' Add the measures together
    ''' </summary>
    ''' <param name="m"></param>
    public function Add(m As Measure)
        Return New Measure(Teaspoons + m.Teaspoons)
    end function

    ''' <summary>
    ''' Multiply the measurement by the value
    ''' </summary>
    ''' <param name="val"></param>
    public Function MultiplyBy(val as Double)
        Return new Measure(Teaspoons * val)
    End Function

    ''' <summary>
    ''' Set the teaspoon values
    ''' </summary>
    ''' <param name="tsp"></param>
    Private sub SetTeaspoons(tsp As Double)
        Teaspoons = Convert.ToInt32(Math.Floor(tsp))
        Dim types = [Enum].GetValues(GetType( MeasureType)).Cast(of MeasureType).OrderByDescending(function(type) type).ToList()
        For Each a As Integer In types
            Dim result = GetValue(tsp, a)
            Values(a) = result.Item1
            tsp = result.Item2
        Next
    End sub

    ''' <summary>
    ''' Get a single value, returning a complex type with the remainder and the whole value of the type
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="type"></param>
    ''' <returns></returns>
    Private Function GetValue(value As double, type As Integer) As Tuple(Of double, double)
        If(value >= type and type > 0)
            Dim newVal = Convert.ToInt32(Math.Floor(value/type))
            Dim remainder = value Mod type
            return New Tuple(Of double,double)(newVal, remainder)
        End If

        Return new Tuple(Of double,double)(0, value)
    End Function

    ''' <summary>
    ''' Ge tthe display name for the measurement
    ''' </summary>
    ''' <param name="mType"></param>
    ''' <returns></returns>
    Private Function GetDisplay(mType As MeasureType) As String
        Dim val = Values(mType)
        If(val <= 0)
            Return ""
        End If

        Dim text = ""
        Select Case mType
            Case MeasureType.Cup
                text = $"{val} Cup(s)"
                Exit Select
            Case MeasureType.CupQuarter
                text = $"1/4 Cup"
                Exit Select
            Case MeasureType.CupHalf
                text = $"1/2 Cup"
                Exit Select
            Case MeasureType.CupThird
                text = $"1/3 Cup"
                Exit Select
            Case MeasureType.CupThreeQuarter
                text = $"3/4 Cup"
                Exit Select
            Case MeasureType.CupTwoThird
                text = $"2/3 Cup"
                Exit Select
            Case MeasureType.Gallon
                text = $"{val} Gallon(s)"
                Exit Select
            Case MeasureType.Pint
                text = $"{val} Pint(s)"
                Exit Select
            Case MeasureType.Quart
                text = $"{val} Quart(s)"
                Exit Select
            Case MeasureType.Tablespoon
                text = $"{val} Tbs"
                Exit Select
            Case MeasureType.Teaspoon
                text = $"{val} Tsp"
                Exit Select
            Case MeasureType.TeaspoonHalf
                text = $"1/2 Tsp"
                Exit Select
            Case MeasureType.TeaspoonQuarter
                text = $"{val}/4 Tsp"
                Exit Select
            Case MeasureType.Pinch
                text = $"A Pinch"
                Exit Select
        End Select

        Return text
    End Function

    ''' <summary>
    ''' Convert this measurement into a human readable string
    ''' </summary>
    ''' <returns></returns>
    Public Overrides Function ToString() As String
        Dim builder = new List(Of string)()
        Dim types = [Enum].GetValues(GetType( MeasureType)).Cast(of MeasureType).OrderByDescending(function(type) type).ToList()
        For Each a As Integer In types
            Dim val = GetDisplay(a)
            if(Not String.IsNullOrWhiteSpace(val))
                builder.Add(val)
            End If
        Next

        Return String.Join(" ", builder)
    End Function
End Class